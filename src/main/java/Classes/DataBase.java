package Classes;

import Interfaces.IDataBase;
import org.neo4j.cypher.ExecutionEngine;
import org.neo4j.cypher.ExecutionResult;
import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.Label;
import org.neo4j.graphdb.Transaction;
import org.neo4j.graphdb.schema.IndexDefinition;
import org.neo4j.graphdb.schema.Schema;
import org.neo4j.kernel.impl.util.StringLogger;

import java.util.Random;
import java.util.concurrent.TimeUnit;

/**
 * Created by maksim on 02.08.14.
 */
public class DataBase implements IDataBase {

    /* округление значения*/
    public static double roundDouble(double d, int n) {
        return Math.round(d * Math.pow(10, (double) n)) / Math.pow(10, (double) n);
    }

    @Override
    public void create_indexes(GraphDatabaseService graphDB, Label label, String field) {
        IndexDefinition indexDefinition;
        try(Transaction tx=graphDB.beginTx()){
            Schema schema=graphDB.schema();
            indexDefinition=schema.indexFor(label).on(field).create();
            tx.success();

        }
        try(Transaction tx=graphDB.beginTx()){
            Schema schema=graphDB.schema();
            schema.awaitIndexOnline(indexDefinition,10, TimeUnit.SECONDS);
            tx.success();
        }
        System.out.println("All indexes are created!");
    }



    @Override
    public void fill_db(GraphDatabaseService graphDB) {
        ExecutionEngine engine=new ExecutionEngine(graphDB, StringLogger.SYSTEM);
        ExecutionResult result;
        try(Transaction tx=graphDB.beginTx()){
            for (double i = 0; i <= 180; i+=0.1) {
                result = engine.execute("CREATE (node: Intervals{ value: "+roundDouble(i,2)
                        +" , indvalue: "+ new Random().nextInt(1000)+" })");       // indvalue это часть ключа, необходимого для идентификации

            }
            System.out.println("DataBase is full!!");
            tx.success();
        }


    }

    public void showAllData(GraphDatabaseService graphDB){
        ExecutionEngine engine=new ExecutionEngine(graphDB,StringLogger.SYSTEM);
        ExecutionResult result;
        try(Transaction tx=graphDB.beginTx()){
            result=engine.execute("MATCH (node:Intervals) return node.value, node.indvalue");
            System.out.println(result.dumpToString());
            tx.success();
        }
    }

    public void deleteAllData(GraphDatabaseService graphDB){
        ExecutionEngine engine=new ExecutionEngine(graphDB,StringLogger.SYSTEM);
        ExecutionResult result;
        try(Transaction tx=graphDB.beginTx()) {

        result=engine.execute("Match (node:Intervals) delete node");
        tx.success();
        }
    }
    public static void deleteRell(GraphDatabaseService graphDB,double[] angles){
        ExecutionEngine engine=new ExecutionEngine(graphDB,StringLogger.SYSTEM);
        ExecutionResult result;
        try(Transaction tx=graphDB.beginTx()){
            for(int i=0;i<angles.length-1;i++){
                result=engine.execute("Match (node:Intervals{value: "+DataBase.roundDouble(angles[i],2)+
                        "})-[r]->(targetnode:Intervals{value:"+DataBase.roundDouble(angles[i+1],2)+"}) delete r");
            }
            tx.success();
        }
    }
}
