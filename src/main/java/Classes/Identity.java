package Classes;

import Interfaces.ISearch;
import org.neo4j.cypher.ExecutionEngine;
import org.neo4j.cypher.ExecutionResult;
import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.Transaction;
import org.neo4j.kernel.impl.util.StringLogger;
import scala.collection.Iterator;

/**
 * Created by maksim on 02.08.14.
 */
public class Identity implements ISearch {
    @Override
    public String search_angles(GraphDatabaseService graphDB,double[] angles) {
        ExecutionEngine engine=new ExecutionEngine(graphDB, StringLogger.SYSTEM);
        ExecutionResult result;
        String ind="";
        Object m;
        try(Transaction tx=graphDB.beginTx()){
            for(int i=0;i<angles.length-1;i++) {
                result = engine.execute("Match (node:Intervals{ value: " +
                        DataBase.roundDouble(angles[i], 2) + "})-[:REF{level:"+i+"}]->(targetnode:Intervals{ value: " +
                        DataBase.roundDouble(angles[i + 1], 2) + "})" +
                        "return (node.indvalue + targetnode.indvalue) as ind");
                // System.out.println(result.dumpToString());}
                Iterator<String> columns = result.columnAs("ind");
                while (columns.hasNext()) {
                    m = columns.next();
                    ind += m.toString();
                }
            }

            tx.success();
            }

        return ind;
    }
/*
    public static void test(GraphDatabaseService graphDB){
        ExecutionEngine engine=new ExecutionEngine(graphDB,StringLogger.SYSTEM);
        ExecutionResult result;
        try(Transaction tx=graphDB.beginTx()){
            result=engine.execute("MATCH (n:Intervals{value: 2.5})-[r:REF]->(t:Intervals{value: 7.8}) return r.level as REF");
            System.out.println(result.dumpToString());
            tx.success();
        }
    }
    */
}
