package Classes;

import Interfaces.IAddNote;
import org.neo4j.cypher.ExecutionEngine;
import org.neo4j.cypher.ExecutionResult;
import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.Transaction;
import org.neo4j.kernel.impl.util.StringLogger;
import scala.collection.Iterator;

/**
 * Created by maksim on 02.08.14.
 */
public class AddNote implements IAddNote {
    @Override
    public String addNewNote(GraphDatabaseService graphDB, double[] angles) {
        int levels=angles.length-1;                                         // число уровней, для параметров отношений
        String ind="";
        Object m;
        ExecutionEngine engine=new ExecutionEngine(graphDB, StringLogger.SYSTEM);
        ExecutionResult result;
        try(Transaction tx=graphDB.beginTx()){
            for(int i=0,j=0;i<levels;i++,j++) {
                result = engine.execute("Match (node:Intervals{ value: "+DataBase.roundDouble(angles[i],2)+"})" +
                        ", (targetnode:Intervals{value: "+DataBase.roundDouble(angles[i+1],2)+"})" +
                        " CREATE UNIQUE (node)-[:REF{ level: "+i+"}]->(targetnode)" +
                        " RETURN (node.indvalue + targetnode.indvalue) as ind;");
                Iterator<String> columns = result.columnAs("ind");
                while(columns.hasNext()){
                    m=columns.next();
                   ind+=m.toString();
                }
            }

            tx.success();
        }

        return ind;
    }
}
