import Classes.AddNote;
import Classes.DataBase;
import Classes.Identity;
import org.neo4j.graphdb.DynamicLabel;
import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.Label;
import org.neo4j.graphdb.factory.GraphDatabaseFactory;

/**
 * Created by maksim on 02.08.14.
 */
public class Example {
    public static void main(String[] args){
        String url="/home/maksim/test/final.db";

        double[] angles={2.5,7.8,9.6,11.6,15.7};
        double[] angles2={2.5,7.8,9.6,11.6,20.4,30.9};
        GraphDatabaseService graphDB=new GraphDatabaseFactory().newEmbeddedDatabase(url);
        DataBase dataBase=new DataBase();
        AddNote addNote=new AddNote();
        Identity identy= new Identity();
        Label label= DynamicLabel.label("Intervals");
        //dataBase.create_indexes(graphDB,label,"value");
        //dataBase.fill_db(graphDB);
        //dataBase.showAllData(graphDB);
       // dataBase.deleteAllData(graphDB);
        //System.out.println("The id of product is: "+ addNote.addNewNote(graphDB, angles2));
        System.out.println("The id of product is: "+ identy.search_angles(graphDB, angles2));
       // Identity.test(graphDB);
      // DataBase.deleteRell(graphDB,angles2);
        graphDB.shutdown();
    }
}
