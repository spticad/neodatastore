package Interfaces;

import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.Label;

/**
 * Created by maksim on 02.08.14.
 */
public interface IDataBase {

    /*Создание индексов для поля в определенной области (вызывается один раз для области)*/
    public void create_indexes(GraphDatabaseService graphDB, Label label,String field);

    /*создание структуры быза дынных*/
    public void fill_db(GraphDatabaseService graphDB);


}
