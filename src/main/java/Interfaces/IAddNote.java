package Interfaces;

import org.neo4j.graphdb.GraphDatabaseService;

/**
 * Created by maksim on 02.08.14.
 */
public interface IAddNote {

    /*регистрирование записи в бд возвращает строку с ключом для доступа к информации*/
    public String addNewNote(GraphDatabaseService graphDB,double[] angles);
}
