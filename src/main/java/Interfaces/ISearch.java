package Interfaces;

import org.neo4j.graphdb.GraphDatabaseService;

/**
 * Created by maksim on 02.08.14.
 */
public interface ISearch {

    public String search_angles(GraphDatabaseService graphDB, double[] angles);
}
